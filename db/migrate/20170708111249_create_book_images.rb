class CreateBookImages < ActiveRecord::Migration[5.1]
  def change
    create_table :book_images do |t|
      t.references :book, foreign_key: true
      t.string :file

      t.timestamps
    end
  end
end
