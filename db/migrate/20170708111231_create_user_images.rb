class CreateUserImages < ActiveRecord::Migration[5.1]
  def change
    create_table :user_images do |t|
      t.references :user, foreign_key: true
      t.string :file

      t.timestamps
    end
  end
end
