class AddAgreementToAuthor < ActiveRecord::Migration[5.1]
  def change
    add_column :authors, :agreement, :boolean, default: false
  end
end
