class AddFavorToAuthor < ActiveRecord::Migration[5.1]
  def change
    add_column :authors, :favor, :boolean, default: false
  end
end
