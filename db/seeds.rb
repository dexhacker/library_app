# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
user = User.create!(name:                  'Farid',
                    email:                 'dexhacker@mail.ru',
                    password:              'qweqwe',
                    password_confirmation: 'qweqwe',
                    admin: true)

User.create!(name:                  'Farid',
             email:                 'qweqwe@mail.ru',
             password:              'qweqwe',
             password_confirmation: 'qweqwe')

count_book = 0
5.times do |counter|
  author = Author.create!(
      name: "Author name #{counter}",
      surname: "Authoer surname #{counter}",
      agreement: true,
      favor: true
  )

  category = Category.create!(title: "Title #{counter}", description: "Description #{counter}")

  5.times do
    count_book += 1
    book = Book.create!(
        user: user,
        title: "Title #{count_book}",
        description: "Description #{count_book}",
        agreement: true,
        favor: true
    )

    AuthorBook.create!(author_id: author.id, book_id: book.id)
    BookCategory.create!(book_id: book.id, category_id: category.id)
    Review.create!(user_id: user.id, book_id: book.id, text: "Text #{count_book}")
  end

end