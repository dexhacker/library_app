class Author < ApplicationRecord
  has_many :author_books
  has_many :books, through: :author_books

  validates :name, presence: true
  validates :surname, presence: true
  validates :agreement, presence: true

  def rating
    result = 0
    count = 0
    self.books.each do |book|
      if book.rating != '' && book.rating != nil && book.rating != 0
        result += book.rating
        count += 1
      end
    end
    return count != 0 ? (result / count).round(1) : 0
  end
end
