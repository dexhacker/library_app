class Book < ApplicationRecord
  belongs_to :user
  has_many :author_books
  has_many :reviews, dependent: :destroy
  has_many :book_categories
  has_many :authors, through: :author_books
  has_many :categories, through: :book_categories
  has_many :book_images

  paginates_per 20

  validates :title, presence: true
  validates :description, presence: true
  validates :agreement, presence: true

  mount_uploader :image, ImageUploader

  def rating
    result = 0
    count = 0
    Rating.where(book_id: self.id).each do |rating|
      if rating.number != '' && rating.number != nil && rating.number != 0
        result += rating.number
        count += 1
      end
    end
    return count != 0 ? (result / count).round(1) : 0
  end
end
