class AuthorsController < ApplicationController
  def index
    @authors = Author.all.where(favor: true)
  end

  def show
    @author = Author.find(params[:id])
    redirect_to root_path, notice: 'Автор не одобрен' if @author.favor == false
  end

  def new
    redirect_to root_path, notice: 'Не зарегестрированые пользователи не могут добавить автора' unless user_signed_in?
    @author = Author.new
  end

  def create
    @author = Author.new(author_params)

    if @author.save
      redirect_to root_path, notice: 'Ваша автор находится на модерации и будет добавления после одобрения администратором'
    else
      render :new
    end
  end

  private

  def author_params
    params.require(:author).permit(:name, :surname, :agreement)
  end
end
