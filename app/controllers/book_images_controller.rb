class BookImagesController < ApplicationController
  def create
    redirect_to root_path, notice: 'Сначало авторизируйтесь!!!' unless user_signed_in?
    p '//////////////////////////////'
    p params[:file]
    book_image = BookImage.create(book_id: params[:id], file: params[:file])
    @file = book_image.file_url(:thumb)
  end
end
