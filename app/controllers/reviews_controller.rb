class ReviewsController < ApplicationController
  def create
    redirect_to root_path, notice: 'Сначало авторизируйтесь!!!' unless user_signed_in?
    Review.create(user_id: current_user.id, book_id: params[:id], text: params[:text])
    @text = params[:text]
    @user = current_user.name
  end
end
