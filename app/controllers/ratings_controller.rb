class RatingsController < ApplicationController
  def create
    redirect_to root_path, notice: 'Сначало авторизируйтесь!!!' unless user_signed_in?
    Rating.create(user_id: current_user.id, book_id: params[:id], number: params[:number])
    @rating = Book.find(params[:id]).rating
    p '///////////////////////////////'
    p @rating
  end
end
