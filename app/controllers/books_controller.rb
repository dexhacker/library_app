class BooksController < ApplicationController
  def index
    if params[:text] != ''
      @books = Book.where("title LIKE ? or description LIKE ?", "%#{params[:text]}%", "%#{params[:text]}%").where(favor: true).page params[:page]
    else
      @books = Book.all.where(favor: true).page params[:page]
    end
  end

  def show
    @book = Book.find(params[:id])
    redirect_to root_path, notice: 'Книга не одобрен' if @book.favor == false
  end

  def new
    redirect_to root_path, notice: 'Не зарегестрированые пользователи не могут добавить книгу' unless user_signed_in?
    @book = Book.new
  end

  def create
    @book = Book.new(book_params)

    if @book.save
      redirect_to root_path, notice: 'Ваша книга находится на модерации и будет добавления после одобрения администратором'
    else
      render :new
    end
  end

  private

  def book_params
    params.require(:book).permit(:title, :description, :agreement, :image, author_ids: [], category_ids: []).merge(user_id: current_user.id)
  end
end
