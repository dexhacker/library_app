Rails.application.routes.draw do

  ActiveAdmin.routes(self)
  root 'books#index'
  resources :books, only: [:index, :show, :new, :create]
  resources :categories, only: [:index, :show]
  resources :authors, only: [:index, :show, :new, :create]
  post '/ratings/:id' => 'ratings#create', as: 'ratings_create'
  post '/reviews/:id' => 'reviews#create', as: 'reviews_create'
  post '/book_images/:id' => 'book_images#create', as: 'book_images_create'


  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
